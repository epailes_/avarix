package problem2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Problem2 {
	private Scanner userInput; //scanner to get users input
	int userValue; //valid user input
	String binaryRepresentation; //integer in binary form
    int numOfOnes; //final value for this problem

	
    public static void main(String[] args) {
        new Problem2().run();
    }     
    
	public void run(){
		userInput = new Scanner(System.in);
		System.out.println("Enter a number between 0 and 255");
		userValue = -1;
		//the following ensures the user only enters a valid number, it must not be out of range, or string etc etc...
		do {
		    try {
		        userValue = userInput.nextInt();
			    if(userValue < 0 || userValue>255){
			        System.out.print("Please only enter a valid number between 0 and 255: ");
			    }
		    } catch (InputMismatchException e) {
		        System.out.print("Please only enter a valid number between 0 and 255: ");
		    }
		    userInput.nextLine(); // clear buffer
		} while (userValue < 0 || userValue>255);
		
		binaryRepresentation = Integer.toBinaryString(userValue);
		System.out.println(binaryRepresentation);
		binaryRepresentation = binaryRepresentation.replace("0", ""); //remove all 0's from the binary
		System.out.println("There are: "+binaryRepresentation.length()+" in the binary form of "+userValue);
	}
}
