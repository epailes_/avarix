package problem3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Problem3 {
	private Scanner userInput; //scanner to get users input
	private int numRows; //valid user input
    private ArrayList<Integer> previousRow = new ArrayList<Integer>();
    private ArrayList<Integer> currentRow = new ArrayList<Integer>();

    public static void main(String[] args) {
        new Problem3().run();
    }     
	
	public void run(){
		userInput = new Scanner(System.in);
		System.out.println("Enter the number of rows you wish to be generated: ");
		numRows = -1;
		//the following ensures the user only enters a valid number, it must not be out of range, or string etc etc...
		do {
		    try {
		        numRows = userInput.nextInt();
			    if(numRows < 0){
			        System.out.print("Please only enter a valid number (must be more than 0): ");
			    }
		    } catch (InputMismatchException e) {
		        System.out.print("Please only enter a valid number (must be more than 0): ");
		    }
		    userInput.nextLine(); // clears the buffer
		} while (numRows < 0);
		
		currentRow.add(1);
		int counter = 1;
		while(numRows>=counter){
			createRow(counter);
			counter++;
		}
	}
	
	@SuppressWarnings("unchecked")
	private void createRow(int rowNum){
		previousRow.clear();
		previousRow = (ArrayList<Integer>) currentRow.clone();
		currentRow.clear();
		int counter = 0;
		int currentNumber = 0;
		while(counter<rowNum){
			if(counter==0){//left most value
				currentNumber = previousRow.get(counter);
				currentRow.add(currentNumber);
			}
			else if(counter==(rowNum-1)){//right most value
				currentNumber = previousRow.get(counter-1);
				currentRow.add(currentNumber);
			}
			else{ //middle values
					currentNumber = previousRow.get(counter-1) + previousRow.get(counter);
					currentRow.add(currentNumber);
			}
			System.out.print(" "+currentNumber+" ");
			counter++;
		}
		System.out.println("");
		
	}
}
